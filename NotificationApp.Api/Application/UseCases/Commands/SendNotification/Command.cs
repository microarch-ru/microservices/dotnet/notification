﻿using MediatR;

namespace NotificationApp.Api.Application.UseCases.Commands.SendNotification
{
    /// <summary>
    /// Отправить нотификацию об изменении статуса заказа
    /// </summary>
    public class Command : IRequest<bool>
    {
        public Guid OrderId { get; }
        public string Status { get; }

        public Command(Guid orderId, string status)
        {
            if (orderId==Guid.Empty) throw new ArgumentException(nameof(orderId));
            if (String.IsNullOrEmpty(status)) throw new ArgumentException(nameof(status));
            
            OrderId = orderId;
            Status = status;
        }
    }
}