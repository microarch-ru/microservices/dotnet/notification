﻿using MediatR;

namespace NotificationApp.Api.Application.UseCases.Commands.SendNotification
{
    public class Handler : IRequestHandler<Command, bool>
    {
        public async Task<bool> Handle(Command message, CancellationToken cancellationToken)
        {
            Console.WriteLine($"Заказ: {message.OrderId}, Статус: {message.Status}");
            return await Task.FromResult(true);
        }
    }
}