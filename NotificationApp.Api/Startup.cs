using MediatR;
using NotificationApp.Api.Adapters.Kafka.OrderStatusChanged;
using NotificationApp.Api.Application.UseCases.Commands.SendNotification;

namespace NotificationApp.Api
{
    public class Startup
    {
        public Startup()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables();
            var configuration = builder.Build();
            Configuration = configuration;
        }

        /// <summary>
        /// Конфигурация
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Health Checks
            services.AddHealthChecks();

            // Configuration
            services.Configure<Settings>(options => Configuration.Bind(options));
            var messageBrokerHost = Configuration["MESSAGE_BROKER_HOST"];
            
            // MediatR
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<Startup>());
            
            // Commands
            services.AddTransient<IRequestHandler<Command, bool>, Handler>();
            
            // Message Broker
            var sp = services.BuildServiceProvider();
            var mediator = sp.GetService<IMediator>();
            services.AddHostedService<ConsumerService>(x => new ConsumerService(mediator,messageBrokerHost));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHealthChecks("/health");
            app.UseRouting();
        }
    }
}